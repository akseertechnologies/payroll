<?php
//session_set_cookie_params((3600 * 24 * 7) + strtotime(date("c")));
ob_start();
error_reporting(0);
if(session_id() == "") 
{
    session_start();
}
ini_set('session.gc_maxlifetime',24 * 60 * 60 * 5);
ini_set('session.bug_compat_42',0);
ini_set('session.bug_compat_warn',0); //test

$CurYear = date("Y") + 10;

header( 'Expires: Mon, 26 Jul ' . $CurYear . ' 05:00:00 GMT' );
header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
header( 'Cache-Control: no-store, no-cache, must-revalidate' );
header( 'Cache-Control: post-check=0, pre-check=0', false );
header( 'Pragma: no-cache' );



$server = "localhost";
$username = "root";
$password = "";
$db_name = "payroll_db";

$link = mysqli_connect($server, $username, $password,$db_name);
if(!$link)
{
	echo "<h3>DB Connection Problem, <br />Error message:</h3>";
	die;
}

$GLOBALS['con'] = $link;

$Con = $GLOBALS['con'];

DEFINE("DB_HOSTNAME",$server);
DEFINE("DB_USERNAME",$username);
DEFINE("DB_PASSWORD",$password);
DEFINE("DB_DATABASE",$db_name);

?>
